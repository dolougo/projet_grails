<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body><center>
    <div class="alert alert-primary" role="alert">
 <a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <div class="card-header">
        <div class="alert alert-danger" role="alert">
        <th>
            <tr>
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>
    </ul>
            </tr>
        </th>
        </div>
    </div>
</div>

    <div class="card">
        <div id="list-user" class="content scaffold-list" role="main">

        <div class="card-header">
         <h1><g:message code="default.list.label" args="[entityName]"/></h1>
        </div>


       <div class="card-body">
            <table class="table table-bordered">



    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
%{--            <f:table collection="${userList}" />--}%


            <thead class="thead-dark">
               <tr>
                    <g:sortableColumn property="username" title="Username"/>

                    <th>
                       Roles
                     </th>

                     <th>
                      Annonces
                     </th>
               </tr>
           </thead>



            <tbody>

              <g:each in="${userList}" var="user">
                  <tr>
                  <g:each in="${user.annonces}" var="annonce">
                      <tr>
                       <g:link controller="user" action="show" id="${user.id}">
                           <td>${user.username}</td>
                       </g:link>

                      <td>${user.getAuthorities().authority.join(",")}</td>

                          <g:link controller="annonce" action="show" id="${annonce.id}">
                              <td>${annonce.title}</td>
                          </g:link>

                      </tr>
                  </g:each>

                  </tr>
              </g:each>

            </tbody>

               <div class="pagination">
                   <g:paginate total="${userCount ?: 0}"/>
                   </div>
            </table>
              </div>
        </div>
   </div>
    </div>
</center>
</body>
</html>