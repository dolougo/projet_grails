<!DOCTYPE html>
<html>

    <head>
    <div class="alert alert-success" role="alert">
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
</div>
    </head>

    <body>
    <div class="alert alert-success" role="alert">

    <center>

        <div class="card" style="width: 25rem;">
            <button>
        <a class="skip" href="#create-user" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
            </button>
</div>
            <div class="nav" role="navigation">
                <div class="card-header">
                    <button>
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
                        </button>
            </div>
        </div>
        <table>
            <tr>

                <div class="card" style="width: 25rem;">
                    <div class="card-body">
        <div id="create-user" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status"> <td>${flash.message}</td></div>
            </g:if>
            <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>

            <g:form resource="${this.user}" method="POST">
                <fieldset class="form">
                    <f:all bean="user"/>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
        </div>
                </div>
        </tr>
        </table>
    </center>
    </div>
    </body>
</div>
</html>
